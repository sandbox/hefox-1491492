<?php

define("SHORTIE_LENGTH", 255);

/**
 * Implements hook_nodeapi().
 */
function shortie_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
  if ($op == 'presave' && variable_get('shortie_' . $node->type, TRUE)) {
    $length = variable_get('shortie_length_' . $node->type, SHORTIE_LENGTH);
    $strip = variable_get('shortie_strip_' . $node->type, TRUE);
    $shortie_field = variable_get('shortie_field_' . $node->type, 'body');
    if ($field = content_fields($shortie_field, $node->type)) {
      $text = $node->{$shortie_field}[0]['value'];
      $format = $field['text_processing'] ? $node->{$shortie_field}[0]['format'] : NULL;
    }
    else {
      $text = $node->body;
      $format = $node->format;
    }
    $node->shortie = shortie_prepare_shortie($text, $format, $length, $strip);
  }
}

/**
 * Prepares the shorted version of the text.
 */
function shortie_prepare_shortie($text, $format, $length = 255, $strip = TRUE) {
  if ($strip) {
    $text = node_teaser($text, $format, $length * 2);
    $text = strip_tags(str_replace(array('</p>', '<br>', '<br />', '<p>'), ' ', $text));
    return drupal_substr($text, 0, $length);
  }
  return node_teaser($text, $format, $length);
}


/**
 * Implements hook_form_FORM_ID_alter() for _node_type_form.
 */
function shortie_form_node_type_form_alter(&$form, &$form_state) {
  // Built in content types do not allow changes to type machine name.
  $type = (isset($form['identity']['type']['#default_value']) ? $form['identity']['type']['#default_value'] : $form['identity']['type']['#value']);

  $form['shortie'] = array(
    '#type' => 'fieldset',
    '#title' => t('Shortie'),
  );

  $form['shortie']['shortie'] = array(
    '#type' => 'checkbox',
    '#title' => t('Store a shortened version of content of this type.'),
    '#default_value' => variable_get('shortie_' . $type, 1),
  );

  $options = array();
  for ($i = 15; $i <= 255; $i++) {
    $options[$i] = $i;
  }

  $form['shortie']['shortie_length'] = array(
    '#type' => 'select',
    '#title' => t('Length'),
    '#default_value' => variable_get('shortie_length_' . $type, SHORTIE_LENGTH),
    '#options' => $options,
  );

  $form['shortie']['shortie_strip'] = array(
    '#type' => 'checkbox',
    '#title' => t('Strip HTML'),
    '#default_value' => variable_get('shortie_strip_' . $type, TRUE),
  );

  // Grab all the fields on this content type.
  $options = array('body' => t('Body'));
  if (module_exists('content')) {
    if ($type_info = content_types($type)) {
      foreach ($type_info['fields'] as $field_name => $field_info) {
        if ($field_info['module'] == 'text') {
          $options[$field_name] = t('CCK field @field', array('@field' => $field_info['widget']['label']));
        }
      }
    }
  }

  $form['shortie']['shortie_field'] = array(
    '#type' => 'select',
    '#title' => t('Field'),
    '#default_value' => variable_get('shortie_field_' . $type, 'body'),
    '#options' => $options,
    '#access' => count($options) > 1,
  );
}


/**
 * Implements hook_node_type().
 */
function shortie_node_type($op, $info) {
  switch ($op) {
    case 'delete':
      foreach (shortie_variables() as $var) {
        variable_del($var . '_' . $info->type);
      }
      break;
    case 'update':
      if (isset($info->old_type)) {
        foreach (shortie_variables() as $var) {
          variable_del($var . '_' . $info->old_type);
        }
      }
  }
}

/**
 * Stores the base of the variables shortie defines.
 */
function shortie_variables() {
  return array(
    'shortie',
    'shortie_field',
    'shortie_strip',
    'shortie_length',
  );
}


/**
 * Implementation of hook_features_pipe_alter() for node component.
 */
function shortie_features_pipe_node_alter(&$pipe, $data, $export, $module_name) {
  if (!empty($data)) {
    foreach ($data as $node_type) {
      foreach (shortie_variables() as $var) {
        $pipe['variable'][] = $var . '_' . $node_type;
      }
    }
  }
}
