<?php

/**
 * @file
 * Hooks for registering custom views handlers
 */

/**
 * Implementation of hook_views_data().
 */
function shortie_views_data() {
  $data['node']['shortie'] = array(
    'field' => array(
      'title' => t('Shortie'),
      'help' => t('Render the shortie text for the node.'),
      'handler' => 'shortie_handler_field_node_shortie',
    ),
  );
  return $data;
}

/**
 * Implementation of hook_views_handlers().
 */
function shortie_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'shortie'),
    ),
    'handlers' => array(
      'shortie_handler_field_node_shortie' => array(
        'parent' => 'views_handler_field',
      ),
    ),
  );
}
