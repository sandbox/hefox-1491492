<?php
/**
 * @file
 * Contains the basic 'node' field handler.
 */

/**
 * Field handler to provide simple renderer that allows linking to a node.
 * Definition terms:
 * - link_to_node default: Should this field have the checkbox "link to node" enabled by default.
 */
class shortie_handler_field_node_shortie extends views_handler_field {

  /**
   * Constructor to provide additional field to add.
   */
  function construct() {
    parent::construct();
    $this->additional_fields['nid'] = array('table' => 'node', 'field' => 'nid');
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['shortie_readmore'] = array('default' => isset($this->definition['shortie_readmore']) ? $this->definition['shortie_readmore'] : TRUE);
    return $options;
  }

  /**
   * Provide link to node option
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['shortie_readmore'] = array(
      '#title' => t('Add readmore link'),
      '#description' => t('Add a readmore link to content.'),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['shortie_readmore']),
    );
  }

  function render($values) {
    // Shortie values are already sanatized (bad idea? meh).
    $value = $values->{$this->field_alias};
    if (!empty($this->options['shortie_readmore'])) {
      $value .= ' ' . l(t('read more'), 'node/' .  $values->{$this->aliases['nid']}, array('attributes' => array('class' => 'readmore shortie-readmore')));
    }
  }
}
